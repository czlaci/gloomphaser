# GloomPhaser

Online implementation of Gloomholdin' game using Phaser.

The game is based on [Gloomholdin'](https://www.boardgamegeek.com/boardgame/340909/gloomholdin), which is owned by [Joe Klipfel](https://www.boardgamegeek.com/boardgamedesigner/135009/joe-klipfel), and is based on the [Gloomhaven](https://www.boardgamegeek.com/boardgame/174430/gloomhaven) game, which is owned by [Cephalofair](http://www.cephalofair.com/).

## Overview

This is an online implementation of the Gloomholdin' card game for those who want to try it out.

The engine of the game is the [Phaser 3](https://phaser.io) platform, which can be used independently via a browser and contains all the necessary elements.

Advantages:

  - No need to print
  - Can be played on both computer and mobile
  - You can record the current status: scenario, level, item, gold

## Elements of the game

Permanent buttons (order): Menu, Zoom in, Zoom out

Card control buttons (order): Flip, Rotate, Depth++, Depth--, Lock (cannot move, but rotate, flip), View

Keyboard shortcuts:

  - Q: Zoom++
  - A: Zoom--
  - F: Flip
  - R: Rotate
  - T: Depth++
  - B: Depth--
  - L: Lock
  - Space: Fullscreen
  - Ctrl: Card view

Menu: The achieved results, objects, locations can be marked. In addition, the game position can be saved (1-4 places) and reloaded. The save is saved to the local storage associated with the website.

## Installation

  - Clone this repository: `git clone git@gitlab.com:czlaci/gloomphaser` or download zip file (and unzip)

  - Open game base html file: `firefox <your game path>/gh.html`

  - If the images do not appear in your browser: [Loading local files in Firefox and Chrome](https://dev.to/dengel29/loading-local-files-in-firefox-and-chrome-m9f)

  - Or copy the files to a local folder on any of your web servers

  - Or try it online here: [Gloomholdin' online](https://czlaci.duckdns.org/gh/gh.html)
