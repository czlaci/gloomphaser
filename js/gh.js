//=============================================================================
// Online Gloomholdin' game
// Written by CzLaci
// Version: 1.0
//=============================================================================
var controls, map;
var drag;
var grpAbi, grpMap, grpMon, grpTra, grpPla;
var grpStat;
var skey, ckey, ekey, spkey, fkey, rkey, tkey, bkey, lkey;
var csprite = null, crec;
var btnFlip, btnRotate, btnDepthP, btnDepthM, btnLock, btnCardZoom, btnZoomPlus, btnZoomMinus, btnLoad, btnSave, btnStatus, btnFullScreen;
var btnCamera;
var statusBg, statusClass;
var infoImg;
var gameSlot = 0;

// Card scale
const SCALE = 0.35;

class Board extends Phaser.Scene {

    constructor() {
        super('Board');
    }

    preload() {
        this.load.image('board', 'images/gh-bg.jpg');
        this.load.spritesheet('gh-abi', 'images/gh-abi.jpg', {frameWidth: 1047, frameHeight: 747});
        this.load.spritesheet('gh-map', 'images/gh-map.jpg', {frameWidth: 747, frameHeight: 1047});
        this.load.spritesheet('gh-mon', 'images/gh-mon.jpg', {frameWidth: 747, frameHeight: 1047});
        this.load.spritesheet('gh-tra', 'images/gh-tra.jpg', {frameWidth: 747, frameHeight: 1047});
        this.load.spritesheet('gh-pla', 'images/gh-pla.png', {frameWidth: 150, frameHeight: 150});
        this.load.spritesheet('gh-rec', 'images/gh-rec.png', {frameWidth: 767, frameHeight: 1067});
        this.load.spritesheet('gh-btn', 'images/gh-btn.png', {frameWidth: 180, frameHeight: 120});
        this.load.spritesheet('gh-sco', 'images/gh-sco.png', {frameWidth: 80, frameHeight: 80});
        this.load.spritesheet('gh-cla', 'images/gh-cla.png', {frameWidth: 200, frameHeight: 60});
        this.load.spritesheet('gh-info', 'images/gh-info.png', {frameWidth: 300, frameHeight: 200});
        this.load.image('gh-sta', 'images/gh-sta.jpg');
    }


    create() {
        this.cameras.main.setBounds(0, 0, 1920, 1200);
        var cursors = this.input.keyboard.createCursorKeys();
        var controlConfig = {
            camera: this.cameras.main,
            left: cursors.left,
            right: cursors.right,
            up: cursors.up,
            down: cursors.down,
            disableCull: true,
            wheel: true,
            zoomIn: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
            zoomOut: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.Q),
            acceleration: 0.05,
            drag: 0.0005,
            maxSpeed: 3.0
        };
        controls = new Phaser.Cameras.Controls.SmoothedKeyControl(controlConfig);
        skey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SHIFT);
        ckey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.CTRL);
        ekey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ESC);
        spkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        fkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.F);
        rkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.R);
        tkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.T);
        bkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.B);
        lkey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L);

        btnStatus = new addButton(this, 50, 2850, 'gh-btn', 10, btnExStatus, this).setVisible(true);
        btnZoomPlus = new addButton(this, 150, 2850, 'gh-btn', 6, btnExZoom, this, 1.1).setVisible(true);
        btnZoomMinus = new addButton(this, 250, 2850, 'gh-btn', 7, btnExZoom, this, 0.9).setVisible(true);
//        btnLoad = new addButton(this, 250, 1850, 'gh-btn', 8, btnExLoad, this).setVisible(true);
//        btnFullScreen = new addButton(this, 1050, 2850, 'gh-btn', 9, gameFullScreen, this).setVisible(true);

        btnCamera = this.cameras.add(0, this.cameras.main.height-100, this.cameras.main.width, this.cameras.main.height);
        btnCamera.scrollY = 2800;

        // Place board
        map = this.add.image(0, 0, 'board').setOrigin(0, 0).setScale(1);

        var card;

        // Map cards
        grpMap = this.add.group();
        for (var i = 0; i < 10; i += 2) {
            card = new cardImage(this, 1700-i*20, 800, 'gh-map', 'm', i);
            if (i == 0) {
                card.setPosition(990, 200);
            }
            card.setLock(true);
        }
        // Monster cards
        grpMon = this.add.group();
        for (var i = 0; i < 12; i += 2) {
            card = new cardImage(this, 1600, 400-i*20, 'gh-mon', 'o', i);
            if (i == 2) {
                card.setPosition(150, 200);
                card.setDepth(110);
            }
            card.setLock(true);
        }
        // Tracker cards
        grpTra = this.add.group();
        for (var i = 0; i < 8; i += 2) {
            card = new cardImage(this, 2400, 520, 'gh-tra', 't', i);
            if (i == 0) {
                card.setPosition(710, 200);
                card.setLock(true);
            } else if (i == 2) {
                card.setPosition(430, 200);
                card.setRotate(180);
                card.setLock(true);
            } else if (i == 4) {
                card.setPosition(1270, 250);
            } else if (i == 6) {
                card.setPosition(1270, 200);
                card.setLock(true);
            }
        }
        // Player tokens
        grpPla = this.add.group();
        for (var i = 0; i < 6; i++) {
            card = new cardImage(this, 950, 470, 'gh-pla', 'p', i);
            if (i == 0) {
                card.setPosition(938, 345);
                card.setDepth(210);
            } else if (i == 1) {
                card.setPosition(1350, 82);
            } else if (i == 2) {
                card.setPosition(1244, 265);
            } else if (i == 3) {
                card.setPosition(1167, 189);
            } else if (i == 5) {
                card.setPosition(950, 540);
                card.setFrame(8);
                card.fn = 8;
                card.cfn = 8;
            }
        }

        // Gold
        for (var i = 0; i < 5; i++) {
            card = new cardImage(this, 1020, 470, 'gh-pla', 'p', 15);
        }

        // Ability cards
        grpAbi = this.add.group();
        for (var i = 0; i < 8; i += 2) {
            card = new cardImage(this, 660-i*32, 580, 'gh-abi', 'a', i);
        }

        // Current card marker
        crec = this.add.image(0, 0, 'gh-rec', 0).setVisible(false).setScale(SCALE);

        // Card property buttons
        btnFlip = new addButton(this, 400, 2850, 'gh-btn', 0, function() {
            csprite.chFlip();
        });
        btnRotate = new addButton(this, 500, 2850, 'gh-btn', 1, function() {
            csprite.chRotate();
        });
        btnDepthP = new addButton(this, 600, 2850, 'gh-btn', 2, function() {
            csprite.setDepth(csprite.depth+1);
            crec.setDepth(csprite.depth+1);
        });
        btnDepthM = new addButton(this, 700, 2850, 'gh-btn', 3, function() {
            csprite.setDepth(csprite.depth-1);
            crec.setDepth(csprite.depth+1);
        });
        btnLock = new addButton(this, 800, 2850, 'gh-btn', 4, function() {
            csprite.chLock();
        });
        btnCardZoom = new addButton(this, 900, 2850, 'gh-btn', 5, function() {
            csprite.zimg.setVisible(true);
        });

        // Default zoom
        this.cameras.main.zoom = 1.0;

        // Mouse wheel
        this.input.on('wheel', function (pointer, gameObj, dx, dy, dz) {
            this.cameras.main.scrollX += dx*0.8;
            this.cameras.main.scrollY += dy*0.5;
        });

        this.input.keyboard.on('keydown', function(key, event) {
            if (spkey.isDown && !game.scale.isFullScreen) game.scale.startFullscreen();
            if (csprite == null) return;
            if (fkey.isDown) csprite.chFlip();
            if (rkey.isDown) csprite.chRotate();
            if (tkey.isDown) csprite.setDepth(csprite.depth+1);
            if (bkey.isDown) csprite.setDepth(csprite.depth-1);
            if (lkey.isDown) csprite.chLock();
            if (ekey.isDown) setBtn(this, false);
        });

        this.scale.on('resize', function (gameSize, baseSize, displaySize, resultion) {
            btnCamera.setPosition(0, displaySize.height-100);
        });

        var xs = 700, ys = 450;
        statusBg = this.add.image(xs, ys, 'gh-sta').setOrigin(0.5, 0.5).setScale(0.5).setDepth(300).setVisible(false);
        statusClass = this.add.image(xs-140, ys-370, 'gh-cla', 0).setOrigin(0.5, 0.5).setScale(0.5).setDepth(301).setVisible(false);

        grpStat = this.add.group();
        card = new addStatusButton(this, xs+170, ys-302, 'gh-cla', 6, 'Load', 0);
        card = new addStatusButton(this, xs+240, ys-302, 'gh-cla', 7, 'Save', 0);

        for (var i = 0; i < 4; i++) {
            card = new addStatusButton(this, xs+145+32*i, ys-370, 'gh-sco', 8+2*i, 'Slot', i);
            if (i == 0) card.chStat();
        }
        for (var i = 0; i < 5; i++) {
            card = new addStatusButton(this, xs-180+32*i, ys-337, 'gh-sco', 8+2*i, 'Level', i);
            if (i == 0) card.chStat();
        }
        for (var i = 0; i < 4; i++) {
            card = new addStatusButton(this, xs+145+32*i, ys-337, 'gh-sco', 6+2*i, 'ScLevel', i);
            if (i == 1) card.chStat();
        }
        for (var i = 0; i < 7; i++) {
            card = new addStatusButton(this, xs-180+36*i, ys-302, 'gh-sco', 18+2*i, 'Gold', i);
        }
        for (var i = 0; i < 4; i++) {
            card = new addStatusButton(this, xs-245, ys-224+32*i, 'gh-sco', 0, 'Upgrade', i);
        }
        for (var i = 0; i < 5; i++) {
            card = new addStatusButton(this, xs-245, ys-43+32*i, 'gh-sco', 0, 'Achievement', i);
        }
        for (var i = 0; i < 11; i++) {
            card = new addStatusButton(this, xs, ys-228+32*i, 'gh-sco', 2, 'Sc_open', i);
        }
        for (var i = 0; i < 11; i++) {
            card = new addStatusButton(this, xs+140, ys-228+32*i, 'gh-sco', 2, 'Sc_open', i+11);
        }
        for (var i = 0; i < 11; i++) {
            card = new addStatusButton(this, xs+35, ys-228+32*i, 'gh-sco', 0, 'Sc_done', i);
        }
        for (var i = 0; i < 11; i++) {
            card = new addStatusButton(this, xs+175, ys-228+32*i, 'gh-sco', 0, 'Sc_done', i+11);
        }
        for (var i = 0; i < 11; i++) {
            card = new addStatusButton(this, xs+70, ys-228+32*i, 'gh-sco', 4, 'Sc_lock', i);
        }
        for (var i = 0; i < 11; i++) {
            card = new addStatusButton(this, xs+210, ys-228+32*i, 'gh-sco', 4, 'Sc_lock', i+11);
        }
        for (var i = 0; i < 8; i++) {
            card = new addStatusButton(this, xs-250, ys+164+30*i, 'gh-sco', 0, 'Item', i);
        }
        for (var i = 0; i < 8; i++) {
            card = new addStatusButton(this, xs-82, ys+164+30*i, 'gh-sco', 0, 'Item', i+8);
        }
        for (var i = 0; i < 8; i++) {
            card = new addStatusButton(this, xs+93, ys+164+30*i, 'gh-sco', 0, 'Item', i+16);
        }
    }

    update (time, delta) {
        // Scroll vidth drag
        if (!drag && this.game.input.activePointer.isDown) {
            if (this.game.origDragPoint) {
                // move the camera by the amount the mouse has moved since last update
                this.cameras.main.scrollX +=
                this.game.origDragPoint.x - this.game.input.activePointer.position.x;
                this.cameras.main.scrollY +=
                this.game.origDragPoint.y - this.game.input.activePointer.position.y;
            } // set new drag origin to current position
            this.game.origDragPoint = this.game.input.activePointer.position.clone();
        } else {
            this.game.origDragPoint = null;
        }
        // Minimal zoom ratio
        if (this.cameras.main.zoom < 0.8) {
            this.cameras.main.zoom = 0.8;
        }
        controls.update(delta);
    }
}


var config = {
    type: Phaser.AUTO,
    width: 1920,
    height: 2000,
    backgroundColor: 'rgba(120, 110, 100, 0)',
    parent: 'phaser-game',
    scene: [ Board ],
    scale: {
        mode: Phaser.Scale.RESIZE
    },
    fps: {              // Game fps
        target: 30,
        min: 2,
        forceSetTimeOut: true
    },
    antialias: true,
    antialiasGL: true,
    roundPixels: true,
    autoRound: true
};

var game = new Phaser.Game(config);

//=============================================================================
// Class of game cards
//=============================================================================
class cardImage extends Phaser.GameObjects.Sprite {
    constructor(_scene, _x, _y, _fname, _group, _frame) {
        super(_scene, _x, _y, _fname);
        _scene.add.existing(this);
        this.setInteractive();
        this.setFrame(_frame);
        this.setName(_fname);
        this.setScale(SCALE);
        this.setOrigin(0.5, 0.5);
        this.cfn = _frame;
        this.fn = _frame;
        this.lock = false;
        this.setDepth(1+_frame);
        this.group = _group;
        this.setVisible(true);
        this.setFlip(false);
        _scene.input.setDraggable(this);
        if (this.group != 'p') {
            this.zimg = _scene.add.image(this.x, this.y, this.name, this.fn);
            this.zimg.setDepth(220).setOrigin(0.5, 0.5).setVisible(false);
            this.clock = _scene.add.image(this.x, this.y, 'gh-rec', 1).setScale(SCALE).setVisible(false);
            this.setCurLock();
        }
        if (this.group == 'a') {
            this.tabIndex = grpAbi.getTotalUsed();
            grpAbi.add(this);
        } else if (this.group == 'm') {
            this.tabIndex = grpMap.getTotalUsed();
            grpMap.add(this);
        } else if (this.group == 'o') {
            this.tabIndex = grpMon.getTotalUsed();
            grpMon.add(this);
        } else if (this.group == 't') {
            this.tabIndex = grpTra.getTotalUsed();
            grpTra.add(this);
        } else if (this.group == 'p') {
            this.tabIndex = grpPla.getTotalUsed();
            grpPla.add(this);
            this.setDepth(200);
        }

        this.on('pointerdown', () => {
            if (statusBg.visible) return;
            if (csprite != null && csprite.zimg.visible) {
                csprite.zimg.setVisible(false);
            }
            if (this.group == 'p') {
                if (this.fn == 4) {
                    if (this.cfn == this.fn) {
                        this.cfn = this.fn+Phaser.Math.Between(1, 3);
                        this.setFrame(this.cfn);
                    } else {
                        this.cfn = this.fn;
                        this.setFrame(this.cfn);
                    }
                }
                if (this.fn == 8) {
                    if (this.cfn == this.fn) {
                        this.cfn = this.fn+Phaser.Math.Between(1, 4);
                        this.setFrame(this.cfn);
                    } else {
                        this.cfn = this.fn;
                        this.setFrame(this.cfn);
                    }
                }
            }
            setBtn(this, true);
        });

        this.on('pointermove', function(pointer, localX, localY, event) {
//           this.zimg.x = localX+this.x/2;
//           this.zimg.y = localY+this.y/2;
            this.updateZoom(_scene);
        });
        this.on('pointerover', function(pointer, localX, localY, event) {
            this.updateZoom(_scene);
        });
        this.on('pointerout', () => {
            if (this.group != 'p') {
                this.zimg.setVisible(false);
            }
        });
        this.on('dragstart', function (pointer, dragX, dragY) {
            if (statusBg.visible) return;
            if (!this.lock) {
                if (this.group != 'p') {
                    this.zimg.setVisible(false);
                }
                if (this.group != 'p') {
                    crec.setDepth(this.depth+1);
                }
                _scene.tweens.add({
                    targets: this,
                    ease: 'Linear',
                    alpha: 0.5,
                    duration: 150,
                });
//                this.alpha = 0.5;
                drag = true;
            }
        });
        this.on('drag', function (pointer, dragX, dragY) {
            if (statusBg.visible) return;
            if (!this.lock) {
                this.x = Math.round(dragX);
                this.y = Math.round(dragY);
                if (this.group != 'p') {
                    crec.setPosition(this.x, this.y);
                }
            }
        });
        this.on('dragend', function (pointer,  dragX, dragY, dropped) {
            if (!this.lock) {
                if (this.group == 'p') {
                    this.setDepth(200);
                } else {
                    crec.setDepth(this.depth-1);
                }
                _scene.tweens.add({
                    targets: this,
                    ease: 'Linear',
                    alpha: 1,
                    duration: 150,
                });
//                this.alpha = 1;
                drag = false;
                setBtn(this, true);
//                console.log(this.x , " : ", this.y);
            }
        });
    }
    updateZoom(scene) {
        if (this.group == 'p') {
            return;
        }
        this.zimg.setScale(Math.min(window.innerHeight, window.innerWidth)/(1150.0*scene.cameras.main.zoom));
        this.zimg.setPosition(scene.cameras.main.scrollX+window.innerWidth/2,
            scene.cameras.main.scrollY+window.innerHeight/2);
        this.zimg.angle = this.angle;
        this.zimg.setFrame(this.cfn);
        if (ckey.isDown) {
            this.zimg.setVisible(true);
        } else {
            this.zimg.setVisible(false);
        }

    }
    chRotate() {
        let dt = this.depth;
        this.depth += 150;
        crec.setVisible(false);
        this.clock.setVisible(false);
        game.scene.scenes[0].tweens.add({
            targets: this,
            ease: 'Linear',
            angle: this.angle+180,
            duration: 300,
            onComplete: () => {
                crec.setVisible(true);
                this.clock.setVisible(this.lock);
                this.depth = dt;
            }
        });
//        this.angle += 180;
        if (this.angle >= 360) {
            this.angle -= 360;
        }
        if (this.zimg != null) {
            this.zimg.angle = this.angle;
        }
    }
    setRotate(angle) {
        this.angle = angle;
        if (this.zimg != null) {
            this.zimg.angle = this.angle;
        }
    }
    chFlip() {
        if (this.flip) {
            let dt = this.depth;
            this.depth += 150;
            let sc = this.scale;
            crec.setVisible(false);
            this.clock.setVisible(false);
            game.scene.scenes[0].tweens.add({
                targets: this,
                ease: 'Linear',
                scaleX: 0.01,
                duration: 150,
                onComplete: () => {
                    this.setFrame(this.fn);
                    game.scene.scenes[0].tweens.add({
                        targets: this,
                        ease: 'Linear',
                        scaleX: sc,
                        duration: 150,
                        onComplete: () => {
                            crec.setVisible(true);
                            this.clock.setVisible(this.lock);
                            this.depth = dt;
                        }
                    });
                }
            });
//            this.setFrame(this.fn);
            this.cfn = this.fn;
            this.flip = false;
        } else {
            let dt = this.depth;
            this.depth += 150;
            let sc = this.scale;
            crec.setVisible(false);
            this.clock.setVisible(false);
            game.scene.scenes[0].tweens.add({
                targets: this,
                ease: 'Linear',
                scaleX: 0.01,
                duration: 150,
                onComplete: () => {
                    this.setFrame(this.fn+1);
                    game.scene.scenes[0].tweens.add({
                        targets: this,
                        ease: 'Linear',
                        scaleX: sc,
                        duration: 150,
                        onComplete: () => {
                            crec.setVisible(true);
                            this.clock.setVisible(this.lock);
                            this.depth = dt;
                        }
                    });
                }
            });
            this.setFrame(this.fn+1);
            this.cfn = this.fn+1;
            this.flip = true;
        }
        if (this.zimg != null) {
            this.zimg.setFrame(this.cfn);
        }
    }
    setFlip (state) {
        if  (state) {
            this.cfn = this.fn+1;
        } else {
            this.cfn = this.fn;
        }
        this.setFrame(this.cfn);
        this.flip = state;
        if (this.zimg != null) {
            this.zimg.setFrame(this.cfn);
        }
    }
    chLock() {
        if (this.lock) {
            this.setLock(false);
        } else {
            this.setLock(true);
        }
    }
    setLock (state) {
        if (state) {
            this.lock = true;
            this.clock.setPosition(this.x, this.y);
            this.clock.setVisible(true);
        } else {
            this.lock = false;
            this.clock.setVisible(false);
        }
    }
    setCurLock() {
        if (this.group != 'p') {
            if (this.height < this.width) {
                this.clock.angle = 90;
            } else {
                this.clock.angle = 0;
            }
        }
    }
}

function setBtn(cs, state) {
    if (state) {
        if (cs.group == 'p') {
            return;
        }
        csprite = cs;
        crec.setPosition(cs.x, cs.y);
        if (cs.height < cs.width) {
            crec.angle = 90;
        } else {
            crec.angle = 0;
        }
        crec.setDepth(cs.depth+1);
    }
    crec.setVisible(state);
    btnFlip.setVisible(state);
    btnRotate.setVisible(state);
    btnDepthP.setVisible(state);
    btnDepthM.setVisible(state);
    btnLock.setVisible(state);
    btnCardZoom.setVisible(state);
    if (!state) csprite = null;
}

//=============================================================================
// Class of game buttons
//=============================================================================
class addButton extends Phaser.GameObjects.Sprite {
    constructor(_scene, _x, _y, _sname, _frame, _click, _param1, _param2) {
        super(_scene, _x, _y, _sname);
        _scene.add.existing(this);
        this.setInteractive();
        this.setVisible(false);
        this.setFrame(_frame);
        this.fn = _frame;
        this.setAlpha(0.3);
        this.setScale(0.5);
        this.on('pointerdown', () => {
            this.setAlpha(0.7);
            _click(_param1, _param2);
            this.setAlpha(0.5);
        });
        this.on('pointerover', () => {
            this.setAlpha(0.5);
        });
        this.on('pointerout', () => {
            this.setAlpha(0.3);
        });
    }
}

//=============================================================================
// Class of Status Buttons
//=============================================================================
class addStatusButton extends Phaser.GameObjects.Sprite {
    constructor(_scene, _x, _y, _sname, _frame, _name, _i) {
        super(_scene, _x, _y, _sname);
        _scene.add.existing(this);
        this.setInteractive();
        this.setVisible(false);
        this.setFrame(_frame);
        this.status = false;
        this.fn = _frame;
        this.name = _name;
        this.i = _i;
        this.setScale(0.45);
        this.setDepth(301);
        this.setOrigin(0.5, 0.5);
        this.tabIndex = grpStat.getTotalUsed();
        grpStat.add(this);
        this.on('pointerdown', () => {
            if (this.name == 'Load') {
                btnExLoad(_scene);
            } else if (this.name == 'Save') {
                btnExSave(_scene);
            } else {
                this.chStat();
            }
        });
        this.on('pointerover', () => {
            this.setAlpha(0.5);
        });
        this.on('pointerout', () => {
            this.setAlpha(1.0);
        });
    }
    chStat() {
        if (this.status) {
            this.setStat(false);
        } else {
            this.setStat(true);
        }
    }
    setStat(stat) {
        if (stat) {
            if (this.name == 'ScLevel') {
                this.clearStat('ScLevel');
            } else if (this.name == 'Slot') {
                this.clearStat('Slot');
                gameSlot = this.i;
            } else if (this.name == 'Level') {
                this.clearStat('Level');
            }
            this.status = true;
            this.setFrame(this.fn+1);
        } else {
            this.status = false;
            this.setFrame(this.fn);
        }
    }
    clearStat(sn) {
        let sc = grpStat.getMatching('name', sn);
        for (let i = 0; i < sc.length; i++) {
            sc[i].setStat(false);
        }
    }
}

function btnExZoom(cs, zf) {
    cs.cameras.main.zoom *= zf;
    if (cs.cameras.main.zoom < 1.05 && cs.cameras.main.zoom > 0.95) {
        cs.cameras.main.zoom = 1.0;
    }
}

function btnExStatus(cs) {
    statusBg.visible ^= 1;
    statusClass.visible ^= 1;
    for (var i = 0; i < grpStat.getLength(); i++) {
        grpStat.getChildren()[i].visible ^= 1;
    }
}

function btnExLoad(sgame) {
    let data = JSON.parse(localStorage.getItem('gh-save-'+gameSlot));
    if (data == null) {
        showInfo(sgame, 2);
        return;
    }
    let grpList = [grpAbi, grpMap, grpMon, grpTra, grpPla];
    let dataList = ['Abi', 'Map', 'Mon', 'Tra', 'Pla']
    for (let j = 0; j < dataList.length; j++) {
        let child = grpList[j].getChildren();
        for (let i = 0; i < grpList[j].getLength(); i++) {
            let item = child[i];
            item.name = data[dataList[j]][i].name;
            item.x = data[dataList[j]][i].x;
            item.y = data[dataList[j]][i].y;
            item.flip = data[dataList[j]][i].flip;
            item.angle = data[dataList[j]][i].angle;
            item.fn = data[dataList[j]][i].fn;
            item.cfn = data[dataList[j]][i].cfn;
            item.scale = data[dataList[j]][i].scale;
            item.visible = data[dataList[j]][i].visible;
            item.depth = data[dataList[j]][i].depth;
            item.active = data[dataList[j]][i].active;
            item.setCurLock();
            if (item.group != 'p') {
                item.setLock(data[dataList[j]][i].lock);
            }
//            item.setFlip(item.flip);
            item.setFrame(item.cfn);
        }
    }
    for (let i = 0; i < grpStat.getLength(); i++) {
        let item = grpStat.getChildren()[i];
        item.setStat(data['Status'][i].status);
    }

    sgame.cameras.main.zoom = data['Zoom'];
    gameSlot = data['GameSlot'];

    showInfo(sgame, 0);
}

function btnExSave(sgame) {
//    localStorage.clear();
    var data = {};
    var grpList = [grpAbi, grpMap, grpMon, grpTra, grpPla];
    var dataList = ['Abi', 'Map', 'Mon', 'Tra', 'Pla']
    for (j = 0; j < dataList.length; j++) {
        data[dataList[j]] = [];
        var child = grpList[j].getChildren();
        for (var i = 0; i < grpList[j].getLength(); i++) {
            var item = child[i];
            data[dataList[j]][i] = { 'name': item.name,
                'x': item.x, 'y': item.y, 'flip': item.flip,
                'angle': item.angle, 'fn': item.fn, 'cfn': item.cfn,
                'scale': item.scale, 'visible': item.visible,
                'depth': item.depth, 'active': item.active, 'lock': item.lock };
        }
    }
    data['Status'] = [];
    for (var i = 0; i < grpStat.getLength(); i++) {
        var item = grpStat.getChildren()[i];
        data['Status'][i] = { 'type': item.type, 'i': item.i, 'status': item.status };
    }
    data['Zoom'] = sgame.cameras.main.zoom;
    data['GameSlot'] = gameSlot;

    localStorage.setItem('gh-save-'+gameSlot, JSON.stringify(data));

    showInfo(sgame, 1);
}

function showInfo(sc, i) {
    if (infoImg == null) {
        infoImg = sc.add.image(sc.cameras.main.centerX+sc.cameras.main.scrollX,
        sc.cameras.main.centerY+sc.cameras.main.scrollY, 'gh-info', i).setOrigin(0.5, 0.5).setScale(0.5).setDepth(310).setVisible(true);
        var timer = sc.time.delayedCall(2000, () => { infoImg.destroy(); infoImg = null; });
    }
}
